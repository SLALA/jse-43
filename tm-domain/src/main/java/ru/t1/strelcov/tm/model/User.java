package ru.t1.strelcov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.enumerated.Role;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
import java.util.Vector;

@Table(name = "tm_user")
@Entity
@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractEntity {

    @Column(unique = true, nullable = false)
    @NotNull
    private String login;

    @Column(name = "password_hash", nullable = false)
    @NotNull
    private String passwordHash;

    @Nullable
    private String email;

    @Column(name = "first_name")
    @Nullable
    private String firstName;

    @Column(name = "last_name")
    @Nullable
    private String lastName;

    @Column(name = "middle_name")
    @Nullable
    private String middleName;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    @NotNull
    private Role role = Role.USER;

    @Column(nullable = false)
    @NotNull
    private Boolean lock = false;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    @NotNull
    public List<Task> tasks = new Vector<>();

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    @NotNull
    public List<Project> projects = new Vector<>();

    public User(@NotNull final String login, @NotNull final String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public User(@NotNull final String login, @NotNull final String passwordHash, @Nullable final String email) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
    }

    public User(@NotNull final String login, @NotNull final String passwordHash, @Nullable Role role) {
        this.login = login;
        this.passwordHash = passwordHash;
        if (role == null) role = Role.USER;
        this.role = role;
    }

    @NotNull
    @Override
    public String toString() {
        return getId() + (login == null || login.isEmpty() ? "" : " : " + login);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        User user = (User) o;
        return login.equals(user.login) && passwordHash.equals(user.passwordHash) && Objects.equals(email, user.email) && Objects.equals(firstName, user.firstName) && Objects.equals(lastName, user.lastName) && Objects.equals(middleName, user.middleName) && role == user.role && lock.equals(user.lock);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), login, passwordHash, email, firstName, lastName, middleName, role, lock);
    }

}
