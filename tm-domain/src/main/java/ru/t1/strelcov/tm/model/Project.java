package ru.t1.strelcov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.Vector;

@Table(name = "tm_project")
@Entity
@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractBusinessEntity {

    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    @NotNull
    public List<Task> tasks = new Vector<>();

    public Project(@NotNull User user, @NotNull String name) {
        super(user, name);
    }

    public Project(@NotNull User user, @NotNull String name, @Nullable String description) {
        super(user, name, description);
    }

}
