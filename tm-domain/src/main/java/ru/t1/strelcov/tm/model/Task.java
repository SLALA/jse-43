package ru.t1.strelcov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.Objects;

@Table(name = "tm_task")
@Entity
@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractBusinessEntity {

    @JoinColumn(name = "project_id", foreignKey = @ForeignKey(name = "fk_project_id"))
    @ManyToOne
    @Nullable
    private Project project;

    public Task(@NotNull User user, @NotNull String name) {
        super(user, name);
    }

    public Task(@NotNull User user, @NotNull String name, @Nullable String description) {
        super(user, name, description);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Task task = (Task) o;
        return Objects.equals(project, task.project);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), project);
    }

}
