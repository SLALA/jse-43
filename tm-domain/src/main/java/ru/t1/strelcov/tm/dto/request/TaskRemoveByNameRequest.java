package ru.t1.strelcov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
@Getter
@Setter
public final class TaskRemoveByNameRequest extends AbstractUserRequest {

    @Nullable
    private String name;

    public TaskRemoveByNameRequest(@Nullable final String token, @Nullable final String name) {
        super(token);
        this.name = name;
    }

}
