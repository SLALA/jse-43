package ru.t1.strelcov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

@WebService
public interface IUserEndpoint {

    @NotNull String SPACE = "http://endpoint.tm.strelcov.t1.ru/";
    @NotNull String PART = "UserEndpointService";
    @NotNull String NAME = "UserEndpoint";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IUserEndpoint newInstance(@NotNull final String host, @NotNull final Integer port) {
        @NotNull final String wsdlURL = "http://" + host + ":" + port + "/" + NAME + "?wsdl";
        @NotNull final URL url = new URL(wsdlURL);
        @NotNull final QName qName = new QName(SPACE, PART);
        return Service.create(url, qName).getPort(IUserEndpoint.class);
    }

    @WebMethod
    @NotNull
    UserChangePasswordResponse changePassword(
            @WebParam(name = "request", partName = "request")
            @NotNull UserChangePasswordRequest request);

    @WebMethod
    @NotNull
    UserListResponse listUser(
            @WebParam(name = "request", partName = "request")
            @NotNull UserListRequest request);

    @WebMethod
    @NotNull
    UserRegisterResponse registerUser(
            @WebParam(name = "request", partName = "request")
            @NotNull UserRegisterRequest request);

    @WebMethod
    @NotNull
    UserRemoveByLoginResponse removeUserByLogin(
            @WebParam(name = "request", partName = "request")
            @NotNull UserRemoveByLoginRequest request);

    @WebMethod
    @NotNull
    UserUpdateByLoginResponse updateUserByLogin(
            @WebParam(name = "request", partName = "request")
            @NotNull UserUpdateByLoginRequest request);

    @WebMethod
    @NotNull
    UserUnlockByLoginResponse unlockByLogin(
            @WebParam(name = "request", partName = "request")
            @NotNull UserUnlockByLoginRequest request);

    @WebMethod
    @NotNull
    UserLockByLoginResponse lockByLogin(
            @WebParam(name = "request", partName = "request")
            @NotNull UserLockByLoginRequest request);

}
