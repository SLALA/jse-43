package ru.t1.strelcov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.strelcov.tm.dto.model.ProjectDTO;
import ru.t1.strelcov.tm.dto.request.ProjectCreateRequest;
import ru.t1.strelcov.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Create project.";
    }

    @Override
    public void execute() {
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER PROJECT NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        @NotNull final ProjectDTO project = projectEndpoint.createProject(new ProjectCreateRequest(getToken(), name, description)).getProject();
        showProject(project);
    }

}
