package ru.t1.strelcov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.bootstrap.Bootstrap;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup implements Runnable {

    @NotNull
    private final static Integer INTERVAL = 30;

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    public Backup(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @SneakyThrows
    @Override
    public void run() {
        save();
    }

    private void load() {
        bootstrap.getDataService().loadBackup();
    }

    private void save() {
        bootstrap.getDataService().saveBackup();
    }

    public void init() {
        if (bootstrap.getPropertyService().getBackupStatus().equals("enabled")) {
            load();
            es.scheduleWithFixedDelay(this, 0, INTERVAL, TimeUnit.SECONDS);
        }
    }

}
